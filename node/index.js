const express = require('express')
const app = express()
const port = 3000

const config = {
    host: 'mysql',
    user: 'root',
    password: 'root',
    database: 'nodedb'
}

const mysql = require('mysql')
const connection = mysql.createConnection(config)

const sql = `INSERT INTO people(name) VALUES('Carlos')`
connection.query(sql)

app.get('/', async (req, res) => {
    let html = '<h1>Fullcycle</h1>'
    connection.query('SELECT name FROM people', (err, rows) => {
        html += '<ul>'
        rows.forEach(row => {
            html += `<li>${row.name}</li>`
        })
        html += '</ul>'
        res.send(html)
    })
})

app.listen(port, () => {
    console.log('Rodando na porta '+port)
})
